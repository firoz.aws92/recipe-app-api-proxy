# Recipe App API proxy

NGNIX proxy app for our recipe app API

## Usage
## Environment Variables

* `LISTEN_PORT` - Port to listen on ( default: `8000`)
* `APP_HOST` - Hostname of the app where the request will be forwarded(default: `app`)
* `APP_PORT` - Port of the app where the request will be forwarded (default: `9000`)

